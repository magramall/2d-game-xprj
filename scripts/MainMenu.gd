extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Quit_pressed():
	get_tree().quit()# Replace with function body.


func _on_Play_pressed():
	Global.loadLevel(1) # Replace with function body.


func _on_Load_pressed():
	Global.load()


func _on_Settings_pressed():
	get_tree().change_scene("res://scenes/SettingsMenu.tscn") # Replace with function body.
