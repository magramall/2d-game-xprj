extends ColorRect


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Quit_pressed():
	get_tree().quit() # Replace with function body.


func _on_Reload_pressed():
	get_tree().reload_current_scene()
	p_vars.hearts = 3
	p_vars.score = 0
	get_tree().paused = false
	get_parent().remove_child(self)# Replace with function body.


func _on_Next_pressed():
	if(Global.next_level_exists):
		Global.save()
		get_tree().paused = false
		Global.nextLevel()
		
