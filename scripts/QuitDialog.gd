extends ConfirmationDialog


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(get_cancel().pressed):
		get_tree().paused = false


func _on_ConfirmationDialog_popup_hide():
		get_tree().paused = false # Replace with function body.


func _on_ConfirmationDialog_focus_exited():
		get_tree().paused = false # Replace with function body.
