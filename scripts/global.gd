extends Node

var current_level = 1;
var next_level = current_level + 1;
var file = File.new()
var next_level_exists = file.file_exists("res://scenes/levels/Level-"+ next_level as String +".tscn")
var data_path = OS.get_user_data_dir()
var difficulty = 0;
signal settings_saved

func _ready():
	get_difficulty()

func _process(delta):
	next_level = current_level + 1
	
func isNextLevelExists():
	return file.file_exists("res://scenes/levels/Level-"+ next_level as String +".tscn")
	
func save_file_exists():
	return file.file_exists("user://savefile.save")

func nextLevel():
	if isNextLevelExists():
		get_tree().change_scene("res://scenes/levels/Level-"+ next_level as String +".tscn")
		current_level += 1
	
func loadLevel(id):
	var checkLevel = file.file_exists("res://scenes/levels/Level-"+ id as String +".tscn")
	if checkLevel:
		get_tree().change_scene("res://scenes/levels/Level-"+ id as String +".tscn")
		current_level = id
		
func get_difficulty():
	var settings_file_exists = file.file_exists("user://settings.conf")
	if settings_file_exists:
		var settings_file = File.new()
		settings_file.open("user://settings.conf", File.READ)
		var data = JSON.parse(settings_file.get_as_text()).result
		difficulty = data.difficulty

func save():
	var save_data = {
	"player_hearts": p_vars.hearts,
	"current_level": next_level,
	"player_score": p_vars.score
	}
	var save_file = File.new()
	save_file.open("user://savefile.save", File.WRITE)
	save_file.store_string(to_json(save_data))
	save_file.close()
	
func delete_save_file():
	var save_file = Directory.new()
	save_file.remove("user://savefile.save")
	
func save_settings():
	var settings_data = {
		"difficulty": difficulty
	}
	var settings_file = File.new()
	settings_file.open("user://settings.conf", File.WRITE)
	settings_file.store_string(to_json(settings_data))
	settings_file.close()
	emit_signal("settings_saved")
	
func load():
	if save_file_exists():
		var save_file = File.new()
		save_file.open("user://savefile.save", File.READ)
		var data = JSON.parse(save_file.get_as_text()).result
		
		p_vars.hearts = data.player_hearts
		p_vars.score = data.player_score
		current_level = data.current_level
		
		loadLevel(current_level)
	

