extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export (int) var run_speed = 100
export (int) var jump_speed = -500
export (int) var gravity = 1200
export (int) var jump_count_max = 2

var velocity = Vector2()
var jumping = false
var jump_count = 0
onready var character = get_node("player")
onready var camera = get_node("player/Camera2D")
onready var score = get_node("player/Camera2D/Score")
onready var health = get_node("player/Camera2D/PlayerHearts")
onready var status = get_node("player/Camera2D/Status")
onready var debug = get_node("player/Camera2D/debug")
var victory = preload("res://scenes/Victory.tscn").instance()

func _ready():
	status.text = "Level " + Global.current_level as String
	
func die():
	if(Global.difficulty == 0):
		p_vars.hearts -= 1
		get_tree().reload_current_scene()
	if Global.difficulty == 1:
		p_vars.hearts -= 1
		p_vars.score = 0
		if p_vars.hearts <= 0:
			Global.loadLevel(1)
			Global.delete_save_file()
			return
		get_tree().reload_current_scene()
		

func get_input():
	velocity.x = 0
	var right = Input.is_action_pressed("ui_right")
	var left = Input.is_action_pressed("ui_left")
	var jump = Input.is_action_just_pressed("ui_jump")
	var escape = Input.is_action_pressed("ui_cancel")

	if jump and ((jump_count < jump_count_max) or is_on_floor()):
		jumping = true
		jump_count += 1
		velocity.y = jump_speed
	if right:
		velocity.x += run_speed
		character.flip_h = false
	if left:
		velocity.x -= run_speed
		character.flip_h = true
	if escape:
		get_tree().change_scene("res://scenes/MainMenu.tscn")

func _physics_process(delta):
	get_input()
	if(p_vars):
		if(p_vars.hearts <= 0):
			p_vars.score = 0
			p_vars.hearts = 3
	velocity.y += gravity * delta
	#dialog.set_position(camera.get_camera_position())
	if(get_slide_count() >= 1):
		var collision = get_slide_collision(0)
		if collision and collision.collider != null:
			if collision.collider.is_in_group("danger"):
				die()
			if collision.collider.is_in_group("score-point"):
				jumping = true
				p_vars.score += 1
				collision.collider.free()
			if collision.collider and collision.collider.is_in_group("level-end"):
				if(not is_a_parent_of(victory)):
					add_child(victory)
					get_tree().paused = true
	if jumping and is_on_floor():
		jumping = false
	if is_on_floor():
		jump_count = 0
	if(position.y >= 1000):
		die()
	velocity = move_and_slide(velocity, Vector2(0, -1))
	score.text = "Score: " + p_vars.score as String
	health.text = "Hearts: " + p_vars.hearts as String
